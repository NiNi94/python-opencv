# OpenCV Experiments

Experiments:

  - **realtimeFeaturesTM.py:** Realtime Image Detection with ORB features
  - **TMCam.py:** Realtime Image Detection with Template Matching + Non-Max Suppression + Multiscale
  - **realtimeFaceDetection.py:** Realtime Face Detection with camera, rtsp stream or mp4 video
  - **featuresAndHoughTransform.py:** Exercices with ORB and Hough Line Transform: 