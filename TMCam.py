import cv2
import numpy as np
import operator

cap = cv2.VideoCapture(0)
template = cv2.imread("jbl.png", cv2.IMREAD_GRAYSCALE)
template = cv2.resize(template, (0, 0), fx=0.2, fy=0.2, interpolation=cv2.INTER_AREA)
template = cv2.flip(template, 1)
w, h = template.shape[::-1]
window_suppression = np.array([False for i in range(0, 5)])
p_old = np.array([0, 0]).astype(int)
wh_old = np.array([0, 0]).astype(int)

while True:
    _, frameRGB = cap.read()
    loc = None
    frameRGB = cv2.resize(frameRGB, (0, 0), fx=0.6, fy=0.6, interpolation=cv2.INTER_AREA)
    frameRGB = cv2.flip(frameRGB, 1)
    frame = cv2.cvtColor(frameRGB, cv2.COLOR_BGR2GRAY)
    found_in_scale = False
    for scale in np.linspace(0.5, 1.0, 3)[::-1]:
        resized = cv2.resize(frame, (0, 0), fx=scale, fy=scale, interpolation=cv2.INTER_AREA)
        r = frame.shape[1] / float(resized.shape[1])
        res = cv2.matchTemplate(resized, template, cv2.TM_CCOEFF_NORMED)
        loc = np.array(np.where(res >= 0.5))
        stacked = np.dstack((loc[1], loc[0]))
        stacked = np.squeeze(stacked, 0)
        if (stacked.size):
            for pt in stacked:
                p_new = (pt * r).astype(int)
                wh_new = np.array([w * r, h * r]).astype(int)
                dist = np.sqrt(np.sum((p_new - p_old) ** 2))
                if (dist > 0 and dist < 100  and found_in_scale == False):
                    found_in_scale = True
                    p_old = p_new
                    wh_old = wh_new
    cv2.rectangle(frameRGB, tuple(p_old) , tuple(wh_old + p_old), (0, 255, 0), 3)
            
    cv2.imshow("Detector", frameRGB)

    key = cv2.waitKey(1)
    if key == 27:
        break

cap.release()
cv2.destroyAllWindows()