import numpy as np
import cv2
import os
from matplotlib import pyplot as plt
from scipy import ndimage
from scipy.spatial import distance
from sklearn.cluster import KMeans

folder=os.listdir(".")
for i in folder:
    print(i)

imgA = cv2.imread('googleG1.png')          # queryImage
imgB = cv2.imread('googleG2.png')         # trainImage
img1 = cv2.cvtColor(imgA,cv2.COLOR_BGR2GRAY)
img2 = cv2.cvtColor(imgB,cv2.COLOR_BGR2GRAY)

# Initiate SIFT detector
brisk = cv2.ORB_create(nfeatures=1000)

# find the keypoints and descriptors with SIFT
kp1, des1 = brisk.detectAndCompute(img1,None)
kp2, des2 = brisk.detectAndCompute(img2,None)

# create BFMatcher object
bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

# Match descriptors.
matches = bf.match(des1,des2)

# Sort them in the order of their distance.
matches = sorted(matches, key = lambda x:x.distance)

out = np.zeros((0,0))
# Draw first 10 matches.
img3 = cv2.drawMatches(imgA,kp1,imgB,kp2,matches[:10],out,flags=2)

# Find the edges in the image using canny detector

edges = cv2.Canny(img2, 50, 200)

# Detect points that form a line

lines = cv2.HoughLinesP(edges, 1, np.pi/180, 60, minLineLength=10, maxLineGap=250)

# Draw lines on the image

for line in lines:

    x1, y1, x2, y2 = line[0]

    cv2.line(imgB, (x1, y1), (x2, y2), (0, 0, 0), 1)
# Show result

cv2.imshow("Result Image", imgB)


plt.imshow(img3)
plt.show()