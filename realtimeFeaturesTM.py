import numpy as np
import cv2
import os
from matplotlib import pyplot as plt
from scipy import ndimage
from scipy.spatial import distance
from sklearn.cluster import KMeans
from numba.typed import List
from numba import njit
from scipy.spatial import distance
from multiprocessing import Pool
import operator

@njit
def centeroidnp(arr):
    length = arr.shape[0]
    sum_x = np.sum(arr[:, 0])
    sum_y = np.sum(arr[:, 1])
    return int(sum_x/length), int(sum_y/length)

cap = cv2.VideoCapture(0)
template = cv2.imread("jbl.png", cv2.IMREAD_COLOR)
template = cv2.resize(template, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
template = cv2.flip(template, 1)
w, h = template.shape[:-1]

# Initiate SIFT detector
orb = cv2.ORB_create(nfeatures=200)

# find the keypoints and descriptors with SIFT
kp1, des1 = orb.detectAndCompute(template, None)
bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
while True:
    _, frameRGB = cap.read()
    loc = None
    frameRGB = cv2.resize(frameRGB, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_AREA)
    frameRGB = cv2.flip(frameRGB, 1)
    frame = frameRGB
    #frame = cv2.cvtColor(frameRGB, cv2.COLOR_BGR2GRAY)
    kp2, des2 = orb.detectAndCompute(frame,None)
    matches = bf.match(des1,des2)
    arr_kp2 = np.array([kp2[mat.trainIdx].pt for mat in matches])
    centroid = centeroidnp(arr_kp2)

    min_distance_outliers = 200
    arr_kp2 = np.array([p for p in arr_kp2 if np.sqrt(np.sum((p - centroid) ** 2) < min_distance_outliers)])
   
    out = np.zeros((0,0))
    #matches = sorted(matches, key = lambda x:x.distance)
    img3 = cv2.drawMatches(template,kp1,frame,kp2,matches[:100],out,flags=2)
    cv2.imshow("Matcher", img3)
    
    if (arr_kp2.size):
        centroid = centeroidnp(arr_kp2)
        rec_size = (80, 80)
        rectangle_vertex_1 = tuple(map(operator.sub, centroid, rec_size))
        rectangle_vertex_2 = tuple(map(operator.add, centroid, rec_size))
        cv2.rectangle(frameRGB, rectangle_vertex_1, rectangle_vertex_2, (0, 255, 0), 3)
    else: pass
    cv2.imshow("Detector", frameRGB)
    
    
    
    
    key = cv2.waitKey(1)
    if key == 27:
        break

cap.release()
cv2.destroyAllWindows()



