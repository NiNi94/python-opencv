import cv2
import numpy as np

face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')

#vcap = cv2.VideoCapture("rtsp://192.168.1.192/ch0_1.h264") #RTSP
#vcap = cv2.VideoCapture('video.mp4') #VIDEO
vcap = cv2.VideoCapture(0) #CAMERA

while(1):
    ret, frame = vcap.read()
    #frame = cv2.resize(frame, (500, 300))
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    for (x,y,w,h) in faces:
        frame = cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
    cv2.imshow('VIDEO', frame)
    key = cv2.waitKey(1)
    if key == 27:
        break

